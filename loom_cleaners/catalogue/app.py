from oscar.apps.catalogue import app
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.decorators import login_required

class CatalogueApplication(app.CatalogueApplication):
    pass

    # def get_url_decorator(self, pattern):
    #     if not settings.OSCAR_ALLOW_ANON_CHECKOUT:
    #         return login_required
    #     if pattern.name.startswith('catalogue'):
    #         return login_required


application = CatalogueApplication()