from django.shortcuts import redirect
from django.urls import reverse

from oscar.core.utils import redirect_to_referrer, safe_referrer
from django.utils.http import is_safe_url

from oscar.apps.basket.views import BasketAddView as DefaultBasketAddView

class BasketAddView(DefaultBasketAddView):

    def get_success_url(self):
        post_url = self.request.GET.get('next')
        if post_url and is_safe_url(post_url, self.request.get_host()):
            return post_url
        return safe_referrer(self.request, 'basket:summary')
