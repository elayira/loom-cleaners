from oscar.apps.basket import config


class BasketConfig(config.BasketConfig):
    name = 'loom_cleaners.basket'
