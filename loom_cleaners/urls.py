from django.conf.urls import include, url
from loom_cleaners.app import application


urlpatterns = [
    url(r'', include(application.urls))
]
