from django.apps import AppConfig


class AboutConfig(AppConfig):
    name = 'loom_cleaners.about'
