from oscar import app
from oscar.core.loading import get_class

from loom_cleaners.promotions import app as promotions_app
from loom_cleaners.checkout import app as checkout_app
from loom_cleaners.catalogue import app as catalogue_app
from loom_cleaners.basket import config as basket_app

from django.conf.urls import include, url


class Shop(app.Shop):
    checkout_app = checkout_app
    promotions_app = promotions_app
    catalogue_app = catalogue_app
    basket_app = basket_app
    def get_urls(self):
        urlpatterns = [
            url(r'', app.application.urls),
        ]
        return urlpatterns


application = Shop()
