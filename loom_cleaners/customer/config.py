from oscar.apps.customer import config


class CustomerConfig(config.CustomerConfig):
    name = 'loom_cleaners.customer'
